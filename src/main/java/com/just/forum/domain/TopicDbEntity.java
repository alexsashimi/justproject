package com.just.forum.domain;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;

@Entity
@Table
public class TopicDbEntity {

    @Id
    @GeneratedValue
    private String id;
    private String title;
    private String description;

    private String creatorPersonId;
    private String commentIds;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatorPersonId() {
        return creatorPersonId;
    }

    public void setCreatorPersonId(String creatorPersonId) {
        this.creatorPersonId = creatorPersonId;
    }

    public List<String> getCommentIds() {
        return Arrays.asList(commentIds.split(","));
    }

    public void setCommentIds(List<String> commentIds) {
        this.commentIds = String.join(",", commentIds);
    }
}