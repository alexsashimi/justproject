package com.just.forum.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
public class CommentDbEntity {

    @Id
    @GeneratedValue
    private String id;

    private String comment;

    private Date date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}