package com.just.forum.domain.repository;

import com.just.forum.domain.CommentDbEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepo extends JpaRepository<CommentDbEntity, String> {
}