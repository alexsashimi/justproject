package com.just.forum.domain.repository;

import com.just.forum.domain.PersonDbEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepo extends JpaRepository<PersonDbEntity, String> {
}