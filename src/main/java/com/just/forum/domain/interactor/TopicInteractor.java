package com.just.forum.domain.interactor;

import com.just.forum.db.entity.TopicEntity;
import com.just.forum.domain.repository.CommentRepo;
import com.just.forum.domain.repository.PersonRepo;
import com.just.forum.domain.repository.TopicRepo;
import org.springframework.beans.factory.annotation.Autowired;

public class TopicInteractor {

    private TopicRepo topicRepo;
    private PersonRepo personRepo;
    private CommentRepo commentRepo;

    @Autowired
    public TopicInteractor(TopicRepo topicRepo, PersonRepo personRepo, CommentRepo commentRepo) {
        this.topicRepo = topicRepo;
        this.personRepo = personRepo;
        this.commentRepo = commentRepo;
    }

    public void saveTopic(TopicEntity topic) {

    }
}
