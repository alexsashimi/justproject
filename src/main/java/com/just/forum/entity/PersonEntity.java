package com.just.forum.entity;

public class PersonEntity {
    public String id;
    public String name;
    public String surname;

    public PersonEntity(String id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }
}
