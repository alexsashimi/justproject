package com.just.forum.entity;

import java.util.List;

public class TopicEntity {
    public String id;
    public String title;
    public String description;
    public PersonEntity creator;
    public List<CommentEntity> comments;
}
