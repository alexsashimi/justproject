package com.just.forum.entity;

import java.util.Date;

public class CommentEntity {
    public String id;
    public String comment;
    public Date date;
    public PersonEntity creator;

    public CommentEntity(String id, String comment, Date date, PersonEntity creator) {
        this.id = id;
        this.comment = comment;
        this.date = date;
        this.creator = creator;
    }
}