package com.just.forum.data.mappers;

import com.just.forum.db.entity.CommentEntity;
import com.just.forum.db.entity.PersonEntity;
import com.just.forum.db.entity.TopicEntity;
import com.just.forum.domain.CommentDbEntity;
import com.just.forum.domain.PersonDbEntity;
import com.just.forum.domain.TopicDbEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TopicMapper {

    public static TopicDbEntity toDbTopic(TopicEntity topic) {
        String id = topic.getId();
        String title = topic.getTitle();
        String description = topic.getDescription();
        PersonDbEntity creator = toDbPerson(topic.getCreator());
        List<CommentDbEntity> comments = new ArrayList<>();
        topic.getComments().forEach(comment -> comments.add(toDbComment(comment)));

        TopicDbEntity result = new TopicDbEntity();
        result.setId(id);
        result.setTitle(title);
        result.setDescription(description);
//        result.setCreator(creator);
//        result.setComments(comments);
        return result;
    }

    public static CommentDbEntity toDbComment(CommentEntity comment) {
        String id = comment.getId();
        String commentText = comment.getComment();
        Date date = comment.getDate();
        PersonDbEntity creator = toDbPerson(comment.getCreator());
        CommentDbEntity result = new CommentDbEntity();
        result.setId(id);
        result.setComment(commentText);
        result.setDate(date);
//        result.setCreator(creator);
        return result;
    }

    public static PersonDbEntity toDbPerson(PersonEntity person) {
        String id = person.getId();
        String name = person.getName();
        String surname = person.getSurname();
        PersonDbEntity result = new PersonDbEntity();
        result.setId(id);
        result.setName(name);
        result.setSurname(surname);
        return result;
    }


    public static TopicEntity dbToEntityTopic(TopicDbEntity topic) {
        String id = topic.getId();
        String title = topic.getTitle();
        String description = topic.getDescription();
//        PersonEntity creator = dbToEntityPerson(topic.getCreator());
//        List<CommentEntity> comments = new ArrayList<>();
//        topic.getComments().forEach(comment -> comments.add(dbToEntityComment(comment)));

        TopicEntity result = new TopicEntity();
        result.setId(id);
        result.setTitle(title);
        result.setDescription(description);
//        result.setCreator(creator);
//        result.setComments(comments);
        return result;
    }

    public static CommentEntity dbToEntityComment(CommentDbEntity comment) {
        String id = comment.getId();
        String commentText = comment.getComment();
        Date date = comment.getDate();
//        PersonEntity creator = dbToEntityPerson(comment.getCreator());
        CommentEntity result = new CommentEntity();
        result.setId(id);
        result.setComment(commentText);
        result.setDate(date);
//        result.setCreator(creator);
        return result;
    }

    public static PersonEntity dbToEntityPerson(PersonDbEntity person) {
        String id = person.getId();
        String name = person.getName();
        String surname = person.getSurname();
        PersonEntity result = new PersonEntity();
        result.setId(id);
        result.setName(name);
        result.setSurname(surname);
        return result;
    }
}
