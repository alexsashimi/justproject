package com.just.forum.controller;

import com.just.forum.db.entity.CommentEntity;
import com.just.forum.db.entity.PersonEntity;
import com.just.forum.db.entity.TopicEntity;
import com.just.forum.exceptions.NotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("comment")
public class CommentController {

    @GetMapping
    public List<CommentEntity> getAllComments(@PathParam("topicId") String topicId) {
        return getTopicById(topicId).getComments();
    }

    @PutMapping("create")
    public void addComment(@PathParam("topicId") String topicId, @RequestBody CommentEntity comment) {
        String newId = UUID.randomUUID().toString();
        String newCommentText = comment.getComment();
        Date createdDate = new Date();
        PersonEntity newPerson = comment.getCreator();

        CommentEntity newComment = new CommentEntity();
        newComment.setId(newId);
        newComment.setComment(newCommentText);
        newComment.setDate(createdDate);
        newComment.setCreator(newPerson);
        getTopicById(topicId).getComments().add(newComment);
    }

    @DeleteMapping("delete")
    public void deleteComment(@PathParam("topicId") String topicId, @PathParam("commentId") String commentId) {
        List<CommentEntity> comments = getTopicById(topicId).getComments();
        for (int i = 0; i < comments.size(); i++) {
            if (comments.get(i).getId().equals(commentId)) {
                comments.remove(i);
                return;
            }
        }
        throw new NotFoundException();
    }

    private TopicEntity getTopicById(String topicId) {
        List<TopicEntity> topicList = getAllTopic();
        TopicEntity currentEntity = null;
        for (int i = 0; i < topicList.size(); i++) {
            if (topicList.get(i).getId().equals(topicId)) {
                currentEntity = topicList.get(i);
                break;
            }
        }
        if (currentEntity != null) {
            return currentEntity;
        }
        throw new NotFoundException();
    }

    private List<TopicEntity> getAllTopic() {
        return TopicController.topicProvider.getAll();
    }
}
