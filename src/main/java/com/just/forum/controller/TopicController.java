package com.just.forum.controller;

import com.just.forum.data.mappers.TopicMapper;
import com.just.forum.db.TopicProvider;
import com.just.forum.db.entity.TopicEntity;
import com.just.forum.domain.TopicDbEntity;
import com.just.forum.domain.repository.TopicRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

//http://localhost:8080/topic

@RestController
@RequestMapping("topic")
public class TopicController {

    private TopicRepo topicRepo;

    @Autowired
    public TopicController(TopicRepo topicRepo) {
        this.topicRepo = topicRepo;
    }

    public static TopicProvider topicProvider = new TopicProvider();

    @GetMapping
    public List<TopicEntity> list() {
        List<TopicEntity> topics = new ArrayList<>();
        topicRepo.findAll().forEach(topicDbEntity -> {
            topics.add(TopicMapper.dbToEntityTopic(topicDbEntity));
        });
        return topics;
    }

    @GetMapping("get")
    public TopicEntity getOneTopic(@PathParam("id") String id) {
        TopicDbEntity dbTopic = topicRepo.findById(id).orElseThrow();
        TopicEntity result = TopicMapper.dbToEntityTopic(dbTopic);
        return result;
    }

    @PutMapping("create")
    public void create(@RequestBody TopicEntity entity) {
        TopicDbEntity result = TopicMapper.toDbTopic(entity);
        topicRepo.save(result);
    }

    @PutMapping("edit")
    public void edit(@RequestBody TopicEntity entity) {
        TopicEntity topicFromDb = getOneTopic(entity.getId());
        BeanUtils.copyProperties(entity, topicFromDb, "id", "creator", "comments");

        TopicDbEntity dbTopic = TopicMapper.toDbTopic(entity);
        topicRepo.save(dbTopic);
    }

    @DeleteMapping
    public void remove(@PathParam("id") String id) {
        topicRepo.deleteById(id);
    }
}